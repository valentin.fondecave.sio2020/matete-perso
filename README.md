## les diagrammes de cas d'utilisation, de classe et de séquence
 
 - Diagramme de Cas d'utilisation
![Image Cas](/Image/cas_utilisation_matete.PNG)
 
 - Diagramme de séquence

![Image sequence](/Image/sequence_matete.PNG)
 
 - Diagramme de classe
 
![Image classe](/Image/diagramme_matete.png)
 
 - Jmerise 
 
![Image mobile](/Image/jmerise.PNG)
 
 
## SYMFONY 
 
 - A quoi sa sert?

    le site permet d'avoir une liste d'annonce de fruit et légume générer par des vendeurs pouvant créer leurs annonces. Cette liste est consultable par des visiteurs ou même les vendeurs.

![Image siteweb](/Image/siteweb.png)
 

 - Comment ca marche ?

    pour les vendeurs ils peuvent créer leur compte et s'y connecter afin de créer leur annonce en y inscrivant dans un formulaire: le titre, la contenu, la date, la catégorie du produit, le vendeur, l'emplacement, on peut modifier notre propre annonce on peut l'ajouter et la supprimer du panier.

![Image vendeur](/Image/vendeur1.png)
 

pour les visiteurs ils peuvent voir la liste de toute les annonces des vendeurs et en ajouter ou supprimer au panier.

![Image visiteur](/Image/visiteur1.png)
 

 - Enregistrement dans la bdd 

    à la fin de chaque modification il est nécessaire de créer une migration via la commande "symfony console make:migration" créant un fichier de version migrations qui s'ajoute dans le dossier de migration, puis lorsque tout les changements sont effectués avec leur migration respective, on peut migrer tout vers la bdd via la commande "symfony console doctrine:migrations:migrate" 

![Image migration](/Image/migration.png) 

![Image migrate](/Image/migration_migrate.png) 
 


- Les démarches
    - on a commencer a créer l'accueil du site sous format html.twig en utilisant bootswatch pour le design ainsi que le menu. 
    - on a ensuite fait la partie controller qui possède toute les fonctions et les routes url du site internet.
    - création de la bdd
    - on a fait la liste d'anonce
    - création d'annonce 
    - inscription et conexion pour le vendeur
    - ajouter ou modifier l'annonce 
    - partie administration
    - ajout du panier d'annonce avec suppression d'annonce 
    - les annonces des vendeurs 
    - permission d'accès au page internet 
