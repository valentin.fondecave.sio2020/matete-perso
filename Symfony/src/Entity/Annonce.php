<?php

namespace App\Entity;

use App\Repository\AnnonceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AnnonceRepository::class)
 */
class Annonce
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=5, minMessage="Votre titre est bien trop court l'ami !")
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     * @Assert\Length(min=5)
     */
    private $Contenu;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;


    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="annonces")
     * @ORM\JoinColumn(nullable=false)
     */
    private $laCategorie;

    /**
     * @ORM\ManyToOne(targetEntity=Emplacement::class, inversedBy="annonce")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Lemplacement;

    /**
     * @ORM\ManyToOne(targetEntity=Vendeur::class, inversedBy="annonces")
     */
    private $levendeur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $quantite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prix;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->Contenu;
    }

    public function setContenu(string $Contenu): self
    {
        $this->Contenu = $Contenu;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }


    public function getLaCategorie(): ?Categorie
    {
        return $this->laCategorie;
    }

    public function setLaCategorie(?Categorie $laCategorie): self
    {
        $this->laCategorie = $laCategorie;

        return $this;
    }

    public function getLemplacement(): ?Emplacement
    {
        return $this->Lemplacement;
    }

    public function setLemplacement(?Emplacement $Lemplacement): self
    {
        $this->Lemplacement = $Lemplacement;

        return $this;
    }

    public function getLevendeur(): ?Vendeur
    {
        return $this->levendeur;
    }

    public function setLevendeur(?Vendeur $levendeur): self
    {
        $this->levendeur = $levendeur;

        return $this;
    }

    public function getQuantite(): ?string
    {
        return $this->quantite;
    }

    public function setQuantite(string $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }
}
