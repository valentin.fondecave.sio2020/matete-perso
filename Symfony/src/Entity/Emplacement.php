<?php

namespace App\Entity;

use App\Repository\EmplacementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmplacementRepository::class)
 */
class Emplacement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $CoordXemplacement;

    /**
     * @ORM\Column(type="float")
     */
    private $CoordYemplacement;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codepostal;

    /**
     * @ORM\OneToMany(targetEntity=Annonce::class, mappedBy="Lemplacement")
     */
    private $annonces;

    /**
     * @ORM\ManyToOne(targetEntity=Vendeur::class, inversedBy="lesEmplacements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $leVendeur;


    public function __construct()
    {
        $this->annonces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCoordXemplacement(): ?float
    {
        return $this->CoordXemplacement;
    }

    public function setCoordXemplacement(float $CoordXemplacement): self
    {
        $this->CoordXemplacement = $CoordXemplacement;

        return $this;
    }

    public function getCoordYemplacement(): ?float
    {
        return $this->CoordYemplacement;
    }

    public function setCoordYemplacement(float $CoordYemplacement): self
    {
        $this->CoordYemplacement = $CoordYemplacement;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodepostal(): ?string
    {
        return $this->codepostal;
    }

    public function setCodepostal(string $codepostal): self
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->setLemplacement($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->removeElement($annonce)) {
            // set the owning side to null (unless already changed)
            if ($annonce->getLemplacement() === $this) {
                $annonce->setLemplacement(null);
            }
        }

        return $this;
    }    

    public function getLeVendeur(): ?Vendeur
    {
        return $this->leVendeur;
    }

    public function setLeVendeur(?Vendeur $leVendeur): self
    {
        $this->leVendeur = $leVendeur;

        return $this;
    }
    
    public function __toString(): string
    {
        return $this->adresse;
    }
}
