<?php

namespace App\Controller\Admin;

use App\Entity\Categorie;
use App\Entity\Annonce;
use App\Entity\Emplacement;
use App\Entity\Vendeur;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator; 
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator; 
use App\Controller\Admin\CategorieCrudController; 
use App\Controller\Admin\AnnonceCrudController; 
use App\Controller\Admin\VendeurCrudController; 
use App\Controller\Admin\EmplacementCrudController; 
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        // redirect to some CRUD controller
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(CategorieCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(AnnonceCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(VendeurCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(EmplacementCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Matete2');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Categories', 'fas fa-list', Categorie::class);
        yield MenuItem::linkToCrud('Emplacement', 'fas fa-list', Emplacement::class);
        yield MenuItem::linkToCrud('Annonces', 'fas fa-list', Annonce::class);
        yield MenuItem::linkToCrud('Vendeur', 'fas fa-list', Vendeur::class);
    }
}
