<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Form\AnnonceType;
use Symfony\Component\Form\Form;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AnnoncesController extends AbstractController
{
private $security;
private $user;
public function __construct(Security $security)
{
$this->security = $security;
$this->user = $security->getUser();
}
    /**
     * @Route("/annonces", name="annonces")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        //requete select * from annonce
        $annonces = $repo->findAll();
        
        return $this->render('annonces/index.html.twig', [
            'controller_name' => 'AnnoncesController',
            'annonces' => $annonces
        ]);
    }

/**
     * @Route("/", name="home")
     */
    public function home() {
        return $this->render('annonces/home.html.twig', [
            'title'=> "Bienvenue ici les amis !",
            'age'=> 31
        ]);
    }
    /**
     * @Route("/annonces/new", name="annonces_create")
     * @Route("/annonces/{id}/edit", name="annonces_edit")
     */

    public function form(Annonce $annonces= null, Request $request, EntityManagerInterface $manager) {
        
        if(!$annonces){
            $annonces = new Annonce();
        }        
                
        $form = $this->createForm(AnnonceType::class, $annonces);
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){ //vérif si envoyé et valide
            //préciser la valeur de createdAt
            if(!$annonces->getId()){
                $annonces->setcreateAt(new \Datetime()) ;
            }
            //sauvegarde de l’article en bdd
            $manager->persist($annonces);
            $manager->flush() ;
            //redirection vers la page de visualisation de l’article
            return $this->redirectToRoute('annonces_show', ['id' =>$annonces->getId()]) ;
        }
	// Afficher le formulaire en passant la vue du formulaire
        return ($this->render('annonces/create.html.twig', [
            'formAnnonces' => $form->createView(),
            'editMode' => $annonces->getId()!==null
        ]));
    }
/**
* @Route("/annonces/delete/{id}", name= "annonce_suppression")
*/
public function supprimerAnnonce(Annonce $annonce=null,Request $requestStack): Response
{
//si l'annonce est trouvée on la supprime
if($annonce->getId() && $this->user == $annonce->getLeVendeur()){
$entityManager = $this->getDoctrine()->getManager();
$entityManager->remove($annonce);
$entityManager->flush();
}
return $this->redirectToRoute('annonces');
}
    /**
     * @Route("/annonces/panier", name="annonces_panier")
     */
    public function panier(){
        //récupération des id d'annonces dans la variable de session panier
        $session=new Session();
        //récupération du panier dans la session s'il existe
        if($session->has('panier')){
            $panier = $session->get('panier');
        }else{
            $panier = array();
        }

        //récupérer les annonces correspondant aux id
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        //requete select * from annonce where id in (....)
        $annonces = $repo->findBy(["id"=>$panier]);

        return $this->render('annonces/panier.html.twig', [
            'annonces'=>$annonces
        ]);
    }
    /**
     * @Route("/annonces/ajout_panier/{id}", name="annonces_ajout")
     */
    public function ajout_panier($id){
        $session=new Session();
        //récupération du panier dans la session s'il existe
        if($session->has('panier')){
            $panier = $session->get('panier');
        }else{
            $panier = array();
        }
        //ajout de l'id dans le panier
        $panier[$id]=$id;
        //mettre à jour le panier dans la session
        $session->set('panier',$panier);
        //renvoyer vers la page panier
        return $this->redirectToRoute('annonces_panier') ;
        
    }
     /**
     * @Route("/annonces/enleve_panier/{id}", name="annonces_enleve")
     */
    public function enleve_panier($id){
        $session=new Session();
        //récupération du panier dans la session s'il existe
        if($session->has('panier')){
            $panier = $session->get('panier');
        }else{ //s'il y a pas de panier, on redirige vers accueil
            return $this->redirectToRoute('annonces') ;
        }
        //suppression de l'id dans le panier
        unset($panier[$id]);
        //mettre à jour le panier dans la session
        $session->set('panier',$panier);
        return $this->redirectToRoute('annonces_panier') ;

    }
        /**
     * @Route("/annonces/payer", name="annonces_payer")
     */
    public function payer(){
        //récupération des id d'annonces dans la variable de session panier
        $session=new Session();
        //récupération du panier dans la session s'il existe
        if($session->has('payer')){
            $payer = $session->get('payer');
        }else{
            $payer = array();
        }

        //récupérer les annonces correspondant aux id
        $repo = $this->getDoctrine()->getRepository(Annonce::class);
        //requete select * from annonce where id in (....)
        $annonces = $repo->findBy(["id"=>$payer]);

        return $this->render('annonces/payer.html.twig', [
            'annonces'=>$annonces
        ]);
    }
    /**
     * @Route("/annonces/{id}", name="annonces_show")
     */
    public function show($id){
        $repo = $this->getDoctrine()->getRepository(Annonce::class);

        $annonces = $repo->find($id);

        return $this->render('annonces/show.html.twig', [
        'annonces' => $annonces
        ]);
    }
    
}
