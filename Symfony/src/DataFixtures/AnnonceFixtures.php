<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\Annonce;
use App\Entity\Vendeur;
use App\Entity\Categorie;
use App\Entity\Emplacement;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AnnonceFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i=1;$i<=3;$i++) {
            $categorie = new Categorie();
            $categorie -> setLibelle('legume');
            
            $manager->persist($categorie);
        }
        $vendeur = new Vendeur();
            $vendeur -> setNom("Pascal");
            $vendeur -> setPrenom("Valentin");
            $vendeur -> setCoordonnee('');
            $vendeur -> setTelephone('');
            $vendeur -> setEmail("user@test.fr");
            $vendeur -> setPassword('totototo');
            $vendeur -> setUsername('user');
            $manager->persist($vendeur);
        for($j=1;$j<=3;$j++) {
            $emplacement = new Emplacement();
            $emplacement -> setAdresse('Perpignan');
            $emplacement -> setCoordXemplacement('42.6844');
            $emplacement -> setCoordYemplacement('2.93708');
            $emplacement -> setCodepostal('66480');
            $emplacement -> setLeVendeur($vendeur);
            $manager->persist($emplacement);
        }
        $emplacement = new Emplacement();
            $emplacement -> setAdresse('Perpignan');
            $emplacement -> setCoordXemplacement('42.6844');
            $emplacement -> setCoordYemplacement('2.93708');
            $emplacement -> setCodepostal('66480');
            $emplacement -> setLeVendeur($vendeur);
            $manager -> persist($emplacement);
        for($k=1;$k<=3;$k++) {
            $vendeur = new Vendeur();
            $vendeur -> setNom("Pascal");
            $vendeur -> setPrenom("Valentin");
            $vendeur -> setCoordonnee('');
            $vendeur -> setTelephone('');
            $vendeur -> setEmail('user'.$k."@test.fr");
            $vendeur -> setPassword('totototo');
            $vendeur -> setUsername('user'.$k);
            $manager->persist($vendeur);
        }
        $categorie = new Categorie();
        $categorie -> setLibelle('legume');
        $manager->persist($categorie) ;
        for($l=1;$l<10;$l++){
            $annonce = new Annonce();
            

            $annonce->setTitre("titre de l'annonce n°$i ")
                ->setContenu("Contenu de l'annonce n°$i")
                ->setQuantite(1)
                ->setPrix(10.0)
                ->setCreateAt(DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s')))
                ->setlaCategorie($categorie)
                ->setlemplacement($emplacement)
                ->setLevendeur($vendeur);

            $manager->persist($annonce) ; //préparer la sauvegarde de l’article
        }
        $manager->flush() ; //lancer les requêtes d’insertion/maj en BDD
    }
}

