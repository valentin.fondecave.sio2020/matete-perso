-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 14 mars 2022 à 16:31
-- Version du serveur : 10.4.22-MariaDB
-- Version de PHP : 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `annoncesmatete`
--
USE annoncesmatete;

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

CREATE TABLE `annonce` (
  `id` int(11) NOT NULL,
  `la_categorie_id` int(11) NOT NULL,
  `lemplacement_id` int(11) NOT NULL,
  `levendeur_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenu` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `annonce`
--

INSERT INTO `annonce` (`id`, `la_categorie_id`, `lemplacement_id`, `levendeur_id`, `titre`, `contenu`, `create_at`) VALUES
(1, 1, 2, 6, 'Pomme de terre', 'Fabriqué en France', '2021-11-30 13:21:00'),
(2, 1, 3, 1, 'Des carottes', 'Des carottes fraiches du matin', '2021-11-30 16:08:00'),
(3, 2, 3, 1, 'Des pommes vertes', 'pommes Françaises', '2021-11-30 16:09:00'),
(4, 1, 4, 3, 'radis', 'radis français', '2021-11-30 16:35:14'),
(6, 1, 2, 6, 'chou-fleur', 'fraichement cuelli', '2022-01-03 15:55:31'),
(7, 1, 2, 3, 'kjnkjn', 'kljhkn', '2022-02-03 14:52:36');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `libelle` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`) VALUES
(1, 'légume'),
(2, 'fruit');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20211129144838', '2021-11-29 15:48:51', 14308);

-- --------------------------------------------------------

--
-- Structure de la table `emplacement`
--

CREATE TABLE `emplacement` (
  `id` int(11) NOT NULL,
  `le_vendeur_id` int(11) NOT NULL,
  `coord_xemplacement` double NOT NULL,
  `coord_yemplacement` double NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codepostal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `emplacement`
--

INSERT INTO `emplacement` (`id`, `le_vendeur_id`, `coord_xemplacement`, `coord_yemplacement`, `adresse`, `codepostal`) VALUES
(2, 6, 42.683, 2.9, 'Perpignan', '66000'),
(3, 1, 42.6844, 2.93708, 'Cabestany', '66330'),
(4, 3, 42.6833, 2.7, 'Le Soler', '66270');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------


--
-- Structure de la table `vendeur`
--

CREATE TABLE `vendeur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coordonnee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `vendeur`
--

INSERT INTO `vendeur` (`id`, `nom`, `prenom`, `coordonnee`, `telephone`, `email`, `roles`, `password`, `username`) VALUES
(1, 'Pascal', 'jean-Pierre', '552', '05454645165', 'toto@toto.fr', '[]', '$2y$13$jPoRHNqr/matReGgCGvbXef73wdVsxHBp982zkGWduQCXI4QNB7Ua', 'toto'),
(3, 'Fondecave', 'Valentin', '455', '0645256223', 'admin@symfony.fr', '[\"ROLE_ADMIN\"]', '$2y$13$X/Q/9w6vh4xJccf.XduKDewa5IDSQ3GsI/bP2ePe.NAa68UfITNxe', 'Valentin'),
(6, 'Ball', 'Fabien', '5522', '0603454488', 'Ball@Fabien.fr', '[]', '$2y$13$2PmKfUFn7eKJT54v6CM30.jkg7Fn6znTArMZHvfuVHiJObmfpkYI.', 'Fabien2');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F65593E5281042B9` (`la_categorie_id`),
  ADD KEY `IDX_F65593E5C5FA1C76` (`lemplacement_id`),
  ADD KEY `IDX_F65593E51108B3B4` (`levendeur_id`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `emplacement`
--
ALTER TABLE `emplacement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C0CF65F6A68C79F5` (`le_vendeur_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`);

--
-- Index pour la table `vendeur`
--
ALTER TABLE `vendeur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7AF49996E7927C74` (`email`),
  ADD UNIQUE KEY `UNIQ_7AF49996F85E0677` (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `annonce`
--
ALTER TABLE `annonce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `emplacement`
--
ALTER TABLE `emplacement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `vendeur`
--
ALTER TABLE `vendeur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD CONSTRAINT `FK_F65593E51108B3B4` FOREIGN KEY (`levendeur_id`) REFERENCES `vendeur` (`id`),
  ADD CONSTRAINT `FK_F65593E5281042B9` FOREIGN KEY (`la_categorie_id`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `FK_F65593E5C5FA1C76` FOREIGN KEY (`lemplacement_id`) REFERENCES `emplacement` (`id`);

--
-- Contraintes pour la table `emplacement`
--
ALTER TABLE `emplacement`
  ADD CONSTRAINT `FK_C0CF65F6A68C79F5` FOREIGN KEY (`le_vendeur_id`) REFERENCES `vendeur` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
