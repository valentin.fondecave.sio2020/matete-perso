<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    public function testSecurity302(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/security');

        $this->assertResponseRedirects("/login", 302);
    }

    public function testnew302(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/annonces/new');

        $this->assertResponseRedirects("/login", 302);
    }

    public function testedit302(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/annonces/1/edit');

        $this->assertResponseRedirects("/login", 302);
    }
}


