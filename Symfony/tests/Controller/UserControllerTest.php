<?php

namespace App\Tests;

use App\Repository\VendeurRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testVisitingWhileLoggedIn()
    {
    $client = static::createClient();
    $VendeurRepository = static::getContainer()->get(VendeurRepository::class);
    // retrieve the test user
    $testUser = $VendeurRepository->findOneByEmail('user@test.fr');
    // simulate $testUser being logged in
    $client->loginUser($testUser);
    // test e.g. the profile page
    $client->request('GET', '/security');
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('h1', 'Hello SecurityController');
    }
}
