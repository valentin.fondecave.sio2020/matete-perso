<?php

namespace App\Tests;

use App\Entity\Annonce;
use PHPUnit\Framework\TestCase;

class AnnonceTest extends TestCase
{
    public function testAssertTrue(): void
    {
        $annonce = new Annonce();
        $date= new \Datetime();
        $annonce->setTitre("le titre");
        $annonce->setContenu("le contenu");
        $annonce->setCreateAt($date);

        $this->assertTrue($annonce->getTitre()==='le titre',"Test du titre");
        $this->assertTrue($annonce->getContenu()==='le contenu',"Test du contenu");
        $this->assertTrue($annonce->getCreateAt()===$date,"Test du CreateAt");
    }
    public function testAssertFalse(): void
    {
        $annonce = new Annonce();
        $date= new \Datetime();
        $this->assertFalse($annonce->getTitre()==='le titre',"Test du titre");
        $this->assertFalse($annonce->getContenu()==='le contenu',"Test du contenu");
        $this->assertFalse($annonce->getCreateAt()===$date,"Test du CreateAt");

    }
    public function testAssertEmpty(): void
    {
        $annonce = new Annonce();
        $date= new \Datetime();
        $this->assertEmpty($annonce->getTitre(),"Test du titre");
        $this->assertEmpty($annonce->getContenu(),"Test du contenu");
        $this->assertEmpty($annonce->getCreateAt(),"Test du CreateAt");
    }
}
